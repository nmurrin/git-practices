**Git Practices**

- https://jameschambers.co/writing/git-team-workflow-cheatsheet/
- https://docs.gitlab.com/ee/workflow/workflow.html
- https://www.atlassian.com/git/tutorials/comparing-workflows/feature-branch-workflow
- https://www.jeffgeerling.com/blogs/jeff-geerling/git-simple-feature-branch
- https://raygun.com/blog/git-workflow/
